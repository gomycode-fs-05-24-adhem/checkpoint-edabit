//Checkpoint portant sur les différentes taches effectuées sur edabits


// création d'un boomrang. il s'agit de créer une fonction count chargée de récupérer le nombre de boomrang dans le array, et d'utiliser la boucle for en définissant i qui au début = 0; ne peut avoir une valeur strictement supérieure au nombre total d'éléments dans le arr; et s'incrémente suivant une certaine condition.
//La condition est la suivante:
function countBoomerangs(arr) 
{
	let count = 0
	for(let i = 0; i<arr.length; i++)
		{
			if(arr[i] !==/*not equal type or not equal value*/ arr [i+1] && arr[i]===arr[i+2])
				{
					count ++
				}
		}
	return count
}

//création d'une fonction de factorisation par 5
function makePlusFunction(baseNum) {
    return  plusFive => baseNum + plusFive
}